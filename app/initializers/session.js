import SessionService from '../services/session';
import Adapter from '../adapters/application';

export function initialize () {

	let app = arguments[1] || arguments[0];

	app.register('service:session', SessionService);

    app.inject('route', 'session', 'service:session');
    app.inject('adapter', 'session', 'service:session');
    app.inject('component', 'session', 'service:session');
    app.inject('controller', 'session', 'service:session');

}

export default {
    name: 'session',
    initialize: initialize
}
