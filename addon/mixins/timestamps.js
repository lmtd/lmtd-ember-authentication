import Ember from 'ember';
import DS from 'ember-data';

export default Ember.Mixin.create({

	created_at: DS.attr('date'),

	modified_at: DS.attr('date'),

	//

	created_at_display: Ember.computed('created_at', function() {

		var date = this.get('created_at');
		return this.convert(date);

	}),

	//

	modified_at_display: Ember.computed('modified_at', function() {

		var date = this.get('modified_at');
		return this.convert(date);

	}),

	convert(input) {

		var date = new Date(input);
		var current = new Date(Date.now());
		var difference = new Date(current - date);

		if ( this.getDayDifference(date, current) < 1 ) {
			if ( difference.getHours()-4 <= 0 ) {
				return difference.getMinutes() + " minutes";
			} else {
				return difference.getHours()-4 + " hours, " + difference.getMinutes() + " minutes";
			}
		} else {
			return (date.getDate()+1) + "/" + (date.getMonth()+1) + "/" + date.getFullYear();
		}

	},

	getDayDifference(d1, d2) {
		var t2 = d2.getTime();
        var t1 = d1.getTime();
        return parseInt((t2-t1)/(24*3600*1000));
	}

});
