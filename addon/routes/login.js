import PublicRoute from './public';

export default PublicRoute.extend({

	actions : {

		submit() {

			var self = this;
			this.session.login().then(function() {

				self.didLoginSuccess();
				self.transitionTo('authenticated');

			}).catch(function(error) {

				self.didLoginFailed();

				if ( error ) {

				}
				// TO HANDLE CERTAIN ERRORS

			});

		},

	},

	//

	didLoginSuccess() {

	},

	didLoginFailed() {

	},

});
