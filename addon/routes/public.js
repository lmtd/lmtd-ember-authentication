import Ember from 'ember';

export default Ember.Route.extend({

	beforeModel(transition) {

		if ( this.session.get('authenticated') ) {

			transition.abort();
			this.transitionTo('authenticated');

		}

	},


});
