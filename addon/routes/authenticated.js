import Ember from 'ember';

export default Ember.Route.extend({

	beforeModel(transition) {

		if ( !this.session.get('authenticated') ) { // IF IT IS NOT AUTHENTICATED

			transition.abort();
			this.session.set('transition', transition); // SAVE TRANSITION FOR FUTURE REDIRECT

			this.transitionTo('login');

		} else { // IF USER IS AUTHENTICATED

			var previoustransition = this.session.get('transition');

			if ( previoustransition ) { // IF TRANSITION WAS SAVED BEFORE

				previoustransition.retry();
				this.session.set('transition', null);

			} else {
	    		if ( this.session.get('completed') || transition.targetName === 'authenticated.index' ) { // IF LOGIN WAS USED

	    			this.session.set('completed', false);
	    			if ( this.get('index') !== transition.targetName ) {
	    				transition.abort();
	    				this.transitionTo(this.get('index'));
	    			}
	    		}

	 		}

		}

	},

	//

	actions: {

		logout() {
			var self = this;
			this.session.logout().then(function() {
				self.didLogout();
				self.transitionTo('login');
			});
		},

		error(error) {
			if ( error.errors ) {
				if ( error.errors[0].status.toString() === '401' ) {
					this.didUnothorized();
					this.session.logout();
					this.transitionTo('login');
				} else {
					return true;
				}
			}
		}

	},

	//

	didLogout() {

	},

	didUnothorized() {

	}


});
