import Ember from 'ember';

export default Ember.Service.extend({

    session: Ember.inject.service(),

    //

    post(attributes, path, authenticate) {

        var data = { data: { attributes: attributes } };

        var headers = { 'Access-Control-Allow-Origin': '*' };

        if ( authenticate ) {
            headers.id = this.get('session.headers.id');
            headers.token = this.get('session.headers.token');
        }

        return this.factory('POST', path, headers, data);

    },

    //

    GET(path, authenticate) {

        if ( authenticate ) {
            return this.factory('GET', path, this.get('session.headers'));
        } else {
            return this.factory('GET', path);
        }

    },

    POST(path, attributes, authenticate) {

        return this.post(attributes, path, authenticate);

    },

    //

    factory(method, path, headers, data) {

        var self = this;

        this.set('processing', true);

        return new Ember.RSVP.Promise(function(resolve, reject) {
    		Ember.$.ajax(
    			{
    				url: self.baseUrl + path,
    				type: method,
    				contentType: 'application/vnd.api+json',
    				data: JSON.stringify(data),
    				dataType: 'json',
    				crossDomain: true,
    				headers: headers,
    			}
    		).done(function(data) {
                resolve(data);
            }).fail(function(error) {
                if ( error.responseJSON ) {
                    reject(error.responseJSON);
                } else {
                    reject(error);
                }
            }).always(function() {
                self.set('processing', false);
            });
        });

    },

    //

    init() {

		var config = this.container.lookupFactory('config:environment');
		this.baseUrl = "/" + config.APP.api_namespace + "/";

	},

});
