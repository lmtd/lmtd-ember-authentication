import Ember from 'ember';
import Credentials from './../models/credentials';
import Passport from './../models/passport';

export default Ember.Service.extend(Ember.Evented, {

	credentials: null,
	passport: null,

	// STATUSES
	authenticated: false,
	failed: false,
  	processing: false,
  	completed: false,

  	// PARAMETERS
  	email: "",
  	password: "",

  	//
  	remember: true,
  	transition: null,
	//

	// PUBLIC API

	requestFactory(attributes, path, method, authenticate) {

		var data = { data: { attributes: attributes } };

		var headers = { 'Access-Control-Allow-Origin': '*' };
		if ( authenticate ) {
			headers.id = this.get('headers.id');
			headers.token = this.get('headers.token');
		}

		return Ember.$.ajax(
			{
				url: this.baseUrl + path,
				type: method,
				contentType: 'application/vnd.api+json',
				data: JSON.stringify(data),
				dataType: 'json',
				crossDomain: true,
				headers: headers,
			}
		);

	},

	//

	request(attributes, path, head) {

		return this.requestFactory(attributes, "auth/" + path, 'POST', head);

	},

	// PASSWORD ----------------------------------------------------------------

	changepassword(password) {

		var self = this;

		return new Ember.RSVP.Promise(function(resolve, reject) {

			self.request({ password: password }, "changepassword", true).done(function(data) {

				self.set('passport.token', data.token);

				resolve(data);

			}).fail(function(data) {
				reject(data);
			});

		});

	},

	resetpassword(email) {

		var self = this;

		return new Ember.RSVP.Promise(function(resolve, reject) {
			self.request({ email: email }, "resetpassword").done(function(data) {
				resolve(data);
			}).fail(function(data) {
				reject(data);
			});
		});

	},

	// LOGIN -------------------------------------------------------------------

	login() {

		var self = this;

		return new Ember.RSVP.Promise(function(resolve, reject) {

			self.set("processing", true);
			self.set("failed", false);

			self.request({ email: self.email, password: self.password}, "login").done(function(data) {

	 			if ( data.id && data.token ) {

	 				// SAVE RESPONSE
	 				self.set('passport.id', data.id);
	 				self.set('passport.token', data.token);

	 				self.set("authenticated", true);

	 				if ( self.remember ) {
	 					self.credentials.set('email', self.get('email'));
	 				}

					Ember.run.later(function() {
						self.set('password', '');
						self.set("processing", false);
					}, 1000);

	 				self.set('completed', true);

	 				self.trigger('login', data.id);

	 				resolve(data);

	 			} else {

	 				self.set("failed", true);

	 				reject(data);

	    		}

	  		}).fail(function(data) { // IF FAILED

				//self.set('password', '');

				self.set("processing", false);
	  			self.set("failed", true);

	  			self.trigger('failed');

	  			reject(data);

	  		});

		});

	},

	// LOGOUT ------------------------------------------------------------------

	logout() {

		var self = this;

		return new Ember.RSVP.Promise(function(resolve) {

	 		self.set('passport.id', null);
	 		self.set('passport.token', null);

	 		self.set("authenticated", false);

	 		self.trigger('logout');

	 		resolve(true);

		});

	},

	// INITIALIZE ------------------------------------------------------------------

	init() {

		var config = this.container.lookupFactory('config:environment');

		this.baseUrl =  "/" + config.APP.api_namespace + "/";

		// CREATE LOCAL STORAGE

		if ( config.APP.storageKeyNamespace ) {

			this.credentials = Credentials.create({ storageKey: 'sq-ember-authentication-credentials-' + config.APP.storageKeyNamespace});
			this.set('passport', Passport.create({ storageKey: 'sq-ember-authentication-passport-' + config.APP.storageKeyNamespace }) );

		} else {

			this.credentials = Credentials.create({ storageKey: 'sq-ember-authentication-credentials'});
			this.set('passport', Passport.create({ storageKey: 'sq-ember-authentication-passport'}));

		}

		// IF AUTH IS NOT EMPTY, LETS ASSUME IT IS VALID
		if ( this.get('passport.id') && this.get('passport.token') ) {

			this.authenticated = true; // SILENT CHANGE

		} else { // IF NOT LOGGED IN, TRY TO PREFILL THE EMAIL

			if ( this.credentials.get('email') ) {
				this.email = this.credentials.get('email');
			}
		}

		this.trigger('init');

	},


	headers : Ember.computed('passport.id', 'passport.token', function() {

		var object = {};
		object.id = this.get('passport.id');
		object.token = this.get('passport.token');

		return object;

	}),

});
