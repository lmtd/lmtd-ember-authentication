import StorageObject from 'ember-local-storage/local/object';

export default StorageObject.extend({

	initialContent: {

		token: null,
		id: null,

	}

});
