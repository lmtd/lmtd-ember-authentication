import StorageObject from 'ember-local-storage/local/object';

export default StorageObject.extend({

	initialContent: {

		email: null,
		password: null,

	}

});
